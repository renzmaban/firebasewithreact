const express = require('express');
const apiCalls = express();
const userFunc = require('../Functions/userFunc')


apiCalls.get('/getUser',(req,res)=>{
  userFunc.getUser((err,result)=>{
    if(err) return res.status(500).send('Internal Server Error')
    return res.status(200).send({result});
  })
});

apiCalls.post('/uploadMaterials',(req,res)=>{
  console.log("reqbody",req.body)
  userFunc.uploadMaterials(req.body,(err,result)=>{
    if (err) return res.status(500).send('Internal Server Error')
    return res.status(200).send({result});
  })
})


module.exports = apiCalls;
const admin = require('firebase-admin');

//const gcs = require('@google-cloud/storage');


const {Storage} = require('@google-cloud/storage');

const storage = new Storage();

const projectId = 'testproject-e6819';


// const storage = new Storage({
//   projectId: projectId,
// });


const bucketName = 'testproject-e6819.appspot.com';

let getAllUsers = (callback) =>{
  admin.database().ref('/user').once('value',snap=>{
    return callback(false,snap.val());
  })
}

let insertStorageMaterials = (data,callback) =>{
  const bucket = storage.bucket(bucketName);
bucket.upload(data.toString('utf8'), {
  destination: "Materials/test",
  gzip:true
}, function(err, file) {
  if (!err) {
    // done
  }
});
}



module.exports = {
  getAllUsers:getAllUsers,
  insertStorageMaterials:insertStorageMaterials
}
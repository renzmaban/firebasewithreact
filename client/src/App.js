import React, { Component } from 'react';
import {Route, BrowserRouter, Switch, Redirect} from 'react-router-dom';
import {authFirebase} from './config';

import * as Utils from './utils'
import Registration from './Component/Register';
import Login from './Component/Login';
import Dashboard from './Component/Dashboard';
import SignUp from './Component/SignUp';
import QRmaker from './Component/QRmaker';
import Materials from './Component/Materials'
import {connect} from 'react-redux'
import _ from 'lodash'
import decode from 'jwt-decode'
import {updateUser} from './js/Actions/userActions'


let payload;

const isAuth = () =>{


  //Temporary
  let loginStatus = sessionStorage.getItem('logged-in');
  let session = sessionStorage.getItem('user-session');

  if(loginStatus){
    payload = decode(session)
    return true
  }else{
    return false
  }

}

const PrivateRoute = ({ component: Component ,...rest  } ) => (
  <Route {...rest} render={props => (
    isAuth() ? (
      <Component {...rest} payload = {payload}/> 
    ) : (
      <Redirect to={{ pathname: '/login' }}/>
    )
  )}/>
)

class App extends Component {
  constructor(props){
    super(props);

  }

  // use location={this.props.location} to get url paramter = GET

  render() {
    console.log("app.js",this.props.user)
    return (
    <BrowserRouter>
    <div className="App">
      <Switch>
        <PrivateRoute exact path = "/Register" component={Registration}/>
        <Route exact path = "/Login" component={Login}/>
        <PrivateRoute exact path = "/Dashboard" component={Login}/>
        <PrivateRoute exact path = "/" component={Dashboard} /> 
        <PrivateRoute exact path = "/QRmaker" component={QRmaker} />  
        <PrivateRoute exact path = "/Materials" component={Materials} /> 
        <Route exact path = "/SignUp" component={SignUp} />
      </Switch>
      </div>
    </BrowserRouter>
    );
  }
}

const mapStateToProps = state =>{
  payload = state.user
  return{
    user:state.user
  }
}

const mapActionsToProps = {
  onUpdateUser:updateUser
};

export default connect(mapStateToProps,mapActionsToProps)(App);

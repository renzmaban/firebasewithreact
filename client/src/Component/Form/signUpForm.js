import React, {Component} from 'react'
import {Form,Input,Button} from 'antd'
import {Firebase,authFirebase} from '../../config'



const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

export default class SignUpForm extends Component{
  constructor(props){
    super(props);

    this.state = {...INITIAL_STATE};
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  
  onSubmit = event => {
    event.preventDefault();
    const { username, email, passwordOne } = this.state;
    console.log("test",passwordOne);

    authFirebase.createUserWithEmailAndPassword(email, passwordOne)
    .then(authUser =>{
      console.log("auth user",authUser);
      this.setState({ ...INITIAL_STATE });
    }).catch(function(err) {  
      // Handle Errors here.
      var errorCode = err.code;
      var errorMessage = err.message;
      console.log("error",err)
      // ...
    });
    

    // Firebase.doCreateUserWithEmailAndPassword(email, passwordOne)
    //   .then(authUser => {
    //     console.log("auth user",authUser)
    //     this.setState({ ...INITIAL_STATE });
    //   })
    //   .catch(error => {
    //     this.setState({ error });
    //   });

    
  };


  render(){
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
    passwordOne !== passwordTwo ||
    passwordOne === '' ||
    email === '' ||
    username === '';

    return(
      // <form onSubmit={this.onSubmit}>
      <Form className = 'example-input' onSubmit={this.onSubmit}>
      <Input
        name="username"
        value={username}
        onChange={this.onChange}
        type="text"
        placeholder="Full Name"
        style={{"marginTop":"10px"}}
        addonBefore = "User Name:"
        size = "large"
      />
      <Input
        name="email"
        value={email}
        onChange={this.onChange}
        type="text"
        placeholder="Email Address"
        style={{"marginTop":"10px"}}
        addonBefore = "Email:"
        size = "large"
      />
      <Input
        name="passwordOne"
        value={passwordOne}
        onChange={this.onChange}
        type="password"
        placeholder="Password"
        style={{"marginTop":"10px"}}
        addonBefore = "Password:"
        size = "large"
      />
      <Input
        name="passwordTwo"
        value={passwordTwo}
        onChange={this.onChange}
        type="password"
        placeholder="Confirm Password"
        style={{"marginTop":"10px"}}
        addonBefore = "Confirm Password:"
        size = "large"
      />
      <Button style={{"marginTop":"30px"}} type="primary" htmlType='submit' block>Sign Up</Button>

      {error && <p>{error.message}</p>}
    </Form>

    )
  }
}
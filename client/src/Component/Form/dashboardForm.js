import React,{Component} from 'react';
import {Modal, Button} from 'antd';
import UpdateModal from '../UserForms/updateModal';
import { updateLocale } from 'moment';
const confirm = Modal.confirm;

export default class dashboardForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      visible: false,
      currentId : 0,
      id:''
    }
  }

  componentDidMount(){
    console.log("props",this.props);
  }

  showModal = (itemId) => {
    this.setState({
      visible: true,
      id:itemId
    });
  }

  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });


    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  }

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  }

  handler = () =>{
    this.setState({
      visible: false,
    });
  }

   showDeleteConfirm(itemId) {
    console.log('delete props',this.props);
    let initProps = this.props
    confirm({
      title: 'Do you Want to delete these items?',
      content: 'Some descriptions',
      onOk() {
        console.log('OK');
        console.log('OK2',initProps);
        initProps.onSubmitDeleteHandler(itemId);
        // {this.props.onSubmitDeleteHandler(itemId)}
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  render(){
    const { visible, confirmLoading} = this.state;
    return(
      <div>
          <Modal title="Title"
              visible={visible}
              destroyOnClose = {true}
              footer = {[null]}
              onCancel={this.handleCancel}
            >
              
              <UpdateModal
                onSubmitUpdateHandler = {this.props.onSubmitUpdateHandler}
                id = {this.state.id}
                handleCancel = {this.handleCancel}

              />

            </Modal>
          
        <ul>
          {this.props.user.map(item => (
            <li key={item.id}>
              The person is {item.name} and the his/her email is {item.email}.
            
            <Button type="primary" onClick={() => this.showModal(item.id)}>
              Update
            </Button>
            <Button onClick={() => this.showDeleteConfirm(item.id)}>
              Delete
            </Button>
            </li>
          ))}
        </ul>
        </div>
    )
  }
}
import React,{Component} from 'react'
import {Link} from 'react-router-dom'

export default class QRmakerForm extends Component{
  constructor(props){
    super(props);
  }
  
  render(){

    return(
      <div>
        <input type = "file" onChange={(e)=>this.props.fileSelectedHandler(e)} multiple/>
        <br></br>
        <button onClick={this.props.fileUploadHandler}>Upload</button>
        <br></br>
        <button onClick={this.props.uploadQr}>Upload QR</button>
        <br></br>
        <Link to="/Materials">Download QR Code</Link>
      </div>
    )
  }


}
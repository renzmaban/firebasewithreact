import React,{Component} from 'react';
import { Form, Input, Button } from 'antd';

const {TextArea} = Input;

export default class registrationForm extends Component{
  constructor(props){
    super(props);
    this.state={
      userInput:{
        username:"",
        password:"",
        name:"",
        email:"",
        address:""
      }
    };



  }

  onChangeHandler = (e) =>{
    let {userInput} = this.state;
    
    switch(e.target.name){
      case 'uUsername':
      console.log("val",e.target.value);
      this.setState({userInput:{userInput,username:e.target.value}})
      // this.setState({userInput:{...userInput,username:e.target.value}})
      console.log("username",userInput.username);
      break;
      case 'uPassword':
      this.setState({userInput:{...userInput,password:e.target.value}})
      console.log("password",this.state.userInput.password);
      break;
      case 'uName':
      this.setState({userInput:{...userInput,name:e.target.value}})
      console.log("name",this.state.userInput.name);
      break;
      case 'uEmail':
      this.setState({userInput:{...userInput,email:e.target.value}})
      console.log("email",this.state.userInput.email);
      break;
      case 'uAddress':
      this.setState({userInput:{...userInput,address:e.target.value}})
      console.log("Address",this.state.userInput.address);
      break;
    }

  }


  render(){
    return(
      <Form className = 'example-input' onSubmit={(e)=>this.props.onSubmitHandler(e, this.state.userInput)}>
        <Input style={{"marginTop":"10px"}} addonBefore = "Name:" onChange={this.onChangeHandler} size="large" name='uName' placeholder="Input Name" />
        <Input style={{"marginTop":"10px"}} addonBefore = "Email:" onChange={this.onChangeHandler} size="large" name='uEmail' placeholder="Input Email Address" />
        <Button style={{"marginTop":"30px"}} type="primary" htmlType='submit' block>Submit</Button>
      </Form>
    )
  }
}
import React,{Component} from 'react'
import { Form, Input, Button, Icon } from 'antd';

const FormItem = Form.Item;

class loginForm extends Component{
  constructor(props){
    super(props)
    this.state={
      login:{
        username:"",
        password:""
      }
    }
  }

  onChangeHandler = (e) =>{
    let {login} = this.state;
    switch(e.target.name){
      case'uUsername':
        this.setState({login:{...login,username:e.target.value}})
        console.log("username",this.state.login.username);
      break;
      case'uUpassword':
        this.setState({login:{...login,password:e.target.value}})
        console.log("password",this.state.login.password);
      break;
    }
  }


  render(){
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return(
      <div style ={{width:"100%",marginLeft: "auto",  marginRight: "auto",minWidth:"100px"}}>
        <Form className='Login-Form' onSubmit= {(e)=> this.props.onSubmitHandler(e,this.state.login,this.props)} layout = "vertical" style ={{width:"100%",marginLeft: "auto",  marginRight: "auto"}}>
          <FormItem
            
          >
            
            {getFieldDecorator('userName', {
                rules: [{
                  type: 'email', message: 'The input is not valid E-mail!',
            },{ required: true, message: 'Please input your username!' },
            {
            type: "regexp",
              pattern: new RegExp("^[0-9]*$"),
              message: "Wrong format!"
            }
          ],
          })(
            <Input addonBefore = "Username:" onChange={this.onChangeHandler} size="large" name='uUsername' placeholder="Input Username" />
            )}
          </FormItem>
          <FormItem
        
          >
            {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your password!' }],
            })(          
          <Input addonBefore = "Password:" type="password" onChange={this.onChangeHandler} size="large" name='uUpassword' placeholder="Input Password" />
           )}
          </FormItem>
          <Button  type="primary" htmlType='submit' block>Submit</Button>
        </Form>
      </div>
    )
  }

}

export default Form.create()(loginForm)
import React,{Component} from 'react'
import HeaderNav from './Layout/Header';
import FooterNav from './Layout/Footer';
import { Layout,Card} from 'antd';
import QRmakerForm from './Form/qrMakerForm'  
import QRious from 'qrious'
import {openNotif} from '../utils'
import {storageFirebase,dbFirebase} from '../config'

const { Content } = Layout;

export default class QRmaker extends Component{
  constructor(props){
    super(props)

    this.state ={
      selectedFile:null,
      qrURL:null,
      sampleqr:null
    }
  }


  fileSelectedHandler = event =>{
    console.log("ev",event.target.files[0])
    // let img = event.target.files[0]

    // img.height = 1080;
    // img.width = 540;
    this.setState({
      selectedFile:event.target.files[0]
    })
  }

  // uploadQr = () =>{
  //   let {selectedFile,qrURL} = this.state
  //   const qr = new QRious();

  //   for(let qrIndex = 0; qrIndex<qrURL.length; qrIndex++){
  //     var qrRef = storageFirebase.ref('QR/test/'+selectedFile[qrIndex].name);

  //     console.log("inside then",selectedFile[qrIndex].name);
  //     qr.value = selectedFile[qrIndex].name+'||'+qrURL[qrIndex]+'||TestCompany"';
  //     qrRef.putString(qr.toDataURL(), 'data_url').then(function(snapshot) {
  //     console.log('Uploaded a data_url string!');
  //     });
  //   }
  //   this.setState({
  //     sampleqr:qr
  //   })
  // }

  fileUploadHandler = () =>{
    
    let {selectedFile} = this.state
    let qrURLtemp = [];

      //for(var i = 0; i<selectedFile.length; i++){
        var storageRef = storageFirebase.ref('Materials/test/'+selectedFile.name);
        storageRef.put(selectedFile).then(snapshot => snapshot.ref.getDownloadURL())
        .then((url) => {
          console.log(url);
          qrURLtemp.push(url);

          console.log("added",url);
          this.setState({
            qrURL:qrURLtemp
          })

        });
     // }

//ORIGINAL

    function start(){
      console.log("url temp",qrURLtemp.length)
      console.log("selected file",selectedFile.length)

      if(qrURLtemp.length != selectedFile.length){
        var time = setTimeout(function(){
          console.log("timeinside")
          start();
        }, 3000);
      
      }
      else{
        clearTimeout(time);
        for(var i = 0; i<qrURLtemp.length; i++){
          let random = Math.floor((Math.random() * 100) + 1);
          let companyId = 'testCompany'+random;

          console.log("array", qrURLtemp);
          dbFirebase.ref('Materials/TestClient/Event01/' + companyId).set({
            Company: companyId,
            DownloadURL: qrURLtemp[i],
            Name:selectedFile[i].name
          });

        }
          openNotif('success',"Upload Successful!", qrURLtemp.length+" Files Uploaded!")
      }
    }

    start();

    //GANAPP TEST

    // function start(){
    //   console.log("url temp",qrURLtemp.length)
    //   //console.log("selected file",selectedFile.length)

    //   if(qrURLtemp.length != 1){
    //     var time = setTimeout(function(){
    //       console.log("timeinside")
    //       start();
    //     }, 3000);
      
    //   }
    //   else{
    //     clearTimeout(time);
    //     for(var i = 0; i<qrURLtemp.length; i++){
    //       let random = Math.floor((Math.random() * 100) + 1);
    //       let companyId = 'testCompany'+random;

    //       console.log("array", qrURLtemp);
    //       dbFirebase.ref('GANAPP/iis018/properties').update({
    //         bannerUrl: qrURLtemp[0],
    //       });

    //     }
    //       openNotif('success',"Upload Successful!", qrURLtemp.length+" Files Uploaded!")
    //   }
    // }

    // start();

  }

  render(){
    return(
      <Layout>
      <HeaderNav/>  
      <Content style={{ padding: '0 10%', marginTop: '150px' ,marginLeft: "auto",  marginRight: "auto"}}>
        <Card
          title="QR Maker"
          style={{ width: '100%' ,  marginLeft: "auto",  marginRight: "auto"}}
        >
          <QRmakerForm
          fileSelectedHandler = {this.fileSelectedHandler}
          uploadQr = {this.uploadQr}
          fileUploadHandler = {this.fileUploadHandler}
          />
        </Card>
       
      </Content>
      <FooterNav/>
    </Layout>
    )
  }
}
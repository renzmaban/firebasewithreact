import React,{Component} from 'react'
import SignUpForm from './Form/signUpForm'
import { Layout,Card} from 'antd';
// import axios from 'axios'
import {serverhost} from '../utils';
import 'antd/dist/antd.css';
import HeaderNav from './Layout/Header';
import FooterNav from './Layout/Footer';


const { Content } = Layout;

export default class SignUp extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return(
      <div>
      <Layout>
        <HeaderNav/>
        <Content style={{ padding: '0 10%', marginTop: '150px' ,marginLeft: "auto",  marginRight: "auto"}}>
          <Card
            title="Sign Up"
            style={{ width: '100%' ,  marginLeft: "auto",  marginRight: "auto"}}
          >
            <SignUpForm
              
            />
          </Card>
        </Content>

        <FooterNav/>
      </Layout>
      
      </div>
    )
  }
}
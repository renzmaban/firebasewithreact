import React,{Component} from 'react'
import { Layout,Menu} from 'antd';
import 'antd/dist/antd.css';
import {Link} from 'react-router-dom'
import {authFirebase} from '../../config'
import {openNotif} from '../../utils'
import {connect} from 'react-redux'
import { updateUser } from '../../js/Actions/userActions';


const { Header } = Layout;



class NavHeader extends Component {
  constructor(props){
    super(props)
  }

  onLogOutHandler = () =>{
    authFirebase.signOut().then(function() {
      // Sign-out successful.
      sessionStorage.removeItem('logged-in');
      openNotif("success","Succesfully Logged Out!","")
      console.log("Succesfuly Signed out!")
    }).catch(function(error) {
      // An error happened.
    });

    this.props.onUpdateUser({})
  }

  render(){
    return(
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%'}}>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['1']}
        style={{ lineHeight: '64px' }}
      >
        
        <Menu.Item key="1"><Link to="/">Dashboard</Link></Menu.Item>
        <Menu.Item key="2"><Link to="/register">Insert User</Link></Menu.Item>
        <Menu.Item key="3"><Link to="/QrMaker">Upload File</Link></Menu.Item>
        <Menu.Item key="6" style= {{float: 'right'}}><Link to ="/Login"  onClick = {this.onLogOutHandler}>LogOut</Link></Menu.Item>
      </Menu>
    </Header>
    )
  }
}

const mapStateToProps = state =>{
  return{
    user:state.user
  }
}

const mapActionsToProps = {
  onUpdateUser:updateUser
}

export default connect(mapStateToProps,mapActionsToProps)(NavHeader)
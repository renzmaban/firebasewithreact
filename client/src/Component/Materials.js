import React,{Component} from 'react'
import {dbFirebase,storageFirebase} from '../config'
import _ from 'lodash';
import HeaderNav from './Layout/Header';
import FooterNav from './Layout/Footer';
import { Layout,Card} from 'antd';
import MaterialsForm from './Materials/materialsForm'
import QRious from 'qrious'
import {openNotif} from '../utils'
import JSZip from 'jszip';
import saveAs from 'file-saver'



const { Content } = Layout;

export default class Materials extends Component{
  constructor(props){
    super(props)
    this.state={
      matData:[{
        Company:null,
        DownloadURL:null,
        Name:null,
        DataURL:null
      }]
    }
  }

  componentDidMount(){
    const qr = new QRious();
    const rootRef = dbFirebase.ref('Materials/TestClient').child('Event01');
    var childObj = {};

    rootRef.on('child_added',snap=>{
      childObj[snap.key] = snap.val();
      qr.value = snap.val().Name+'||'+snap.val().DownloadURL+'||'+snap.val().Company;
      childObj[snap.key].DataURL = qr.toDataURL();

      console.log("data URL", qr.toDataURL());
      console.log("fsdfsdf",childObj);
      this.setState({
        matData:_.values(childObj)
      });
    });

    rootRef.on('child_changed',snap=>{
     childObj[snap.key] = snap.val();
      qr.value = snap.val().Name+'||'+snap.val().DownloadURL+'||'+snap.val().Company;
      childObj[snap.key].DataURL = qr.toDataURL();

      console.log("data URL", qr.toDataURL());
      console.log("fsdfsdf",childObj);
      this.setState({
        matData:_.values(childObj)
      });
    });

    rootRef.on('child_removed',snap=>{
      delete childObj[snap.key];
      console.log("changed",snap.val())
      this.setState({
        matData:_.values(childObj)
      });
    });
  }

  DeleteFile = (item) => {
      // Create a reference to the file to delete
    var desertRef = storageFirebase.ref('Materials/test/'+item.Name);
      // Delete the file
    desertRef.delete().then(function() {
      // File deleted successfully
    }).catch(function(error) {
      // Uh-oh, an error occurred!
    });
    dbFirebase.ref('Materials/TestClient/Event01').child(item.Company).remove();
    openNotif('success',"Delete Successful!","File Deleted!")
  }

  DownloadAll = () =>{
    let {matData} = this.state

    console.log("matdata",this.state);
    var arrURL = [];
    var arrName = [];
    for (var key in matData) {
      arrURL.push(matData[key].DataURL);
      arrName.push(matData[key].Name);
    }
    
    console.log("arr",arrURL);
    var zip = new JSZip();
 
    // Generate a directory within the Zip file structure
    var img = zip.folder("Qr Codes");

    // Add a file to the directory, in this case an image with data URI as contents
    for(let count = 0 ; count < arrURL.length; count ++){
      img.file(arrName[count], arrURL[count].split(',')[1], {base64: true});

    }

    // Generate the zip file asynchronously
    zip.generateAsync({type:"blob"})
    .then(function(content) {
        // Force down of the Zip file
        saveAs(content, "archive.zip");
    });

  }


  render(){
    console.log("props",this.props.payload)
    return(
      <Layout>
      <HeaderNav/>  
      <Content style={{ padding: '0 10%', marginTop: '150px' ,marginLeft: "auto",  marginRight: "auto"}}>
        <Card
          title="Materials List "
          style={{ width: '100%' ,  marginLeft: "auto",  marginRight: "auto"}}
        >
          <MaterialsForm
            matData= {this.state.matData}
            DeleteFile = {this.DeleteFile}
            DownloadAll = {this.DownloadAll}
          />
        </Card>
       
      </Content>
      <FooterNav/>
    </Layout>

    )
  }


}
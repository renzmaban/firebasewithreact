import React,{Component} from 'react';
import DashBoardForm from './Form/dashboardForm';
import {dbFirebase,authFirebase} from '../config';
import HeaderNav from './Layout/Header';
import FooterNav from './Layout/Footer';
import { Layout,Card} from 'antd';
import _ from 'lodash';
import {openNotif} from '../utils'
import axios from 'axios'

import {connect} from 'react-redux'

const {Content} = Layout;

class Dashboard extends Component{
  constructor(props){
    super(props)
    this.state = {
      user:[{
        id:'test',
        name:'test',
        email:'test'
      }]
    }
  }

  componentWillMount(){
  //   console.log("componend will mount")
  //   axios.get('https://us-central1-testproject-e6819.cloudfunctions.net/testAPI')
  // .then(function (response) {
  //   // handle success
  //   console.log("axios res",response);
  // })
  // .catch(function (error) {
  //   // handle error
  //   console.log(error);
  // })
  // .then(function () {
  //   // always executed
  // });


  //   const eventSource = new EventSource('https://us-central1-testproject-e6819.cloudfunctions.net/testAPI');

  //   eventSource.onmessage =(e) => {
  //     console.log(e.data);
  //     console.log(" whole e",e)
  //   };
  }

  componentDidMount(){

    const rootRef = dbFirebase.ref().child('user');
    const nameRef = rootRef.child('1');
    var childObj = {};
    rootRef.on('child_added',snap=>{
      childObj[snap.key] = snap.val();
      this.setState({
        user:_.values(childObj)
      });
    });
    rootRef.on('child_changed',snap=>{
      childObj[snap.key] = snap.val();
      this.setState({
        user:_.values(childObj)
      });
    });

    rootRef.on('child_removed',snap=>{
      delete childObj[snap.key];
      this.setState({
        user:_.values(childObj)
      });
    });

    //Object.values(snap.val())
  }

  onSubmitUpdateHandler = (e,userEntries,propsData) =>{
    e.preventDefault();
    
    console.log("props",propsData)

    // Get a key for a new Post.
    // var newPostKey = dbFirebase.ref().child('user').push().key;
    // console.log("newpostkey?",newPostKey)
    //This just generate new key

    dbFirebase.ref('user/' + userEntries.id).set({
      name:userEntries.name,
      email:userEntries.email
    });
    openNotif('success',"Update Successful!","User has been updated")
    this.handleCanceler(e,propsData);
  }

  onSubmitDeleteHandler = (itemId) =>{
    dbFirebase.ref('user').child(itemId).remove();
    openNotif('success',"Delete Successful!","User Deleted!")
  }
  handleCanceler = (e,settingsProps) =>{
    settingsProps.handleCancel();
  }

  render(){
    console.log("propsss",this.props)
    return(
      <Layout>
        <HeaderNav></HeaderNav>
        <Content style={{ padding: '0 10%', marginTop: '150px' ,marginLeft: "auto",  marginRight: "auto"}}>
          <Card
            title="Login"
            style={{ width: '100%' ,  marginLeft: "auto",  marginRight: "auto"}}
          >
            <DashBoardForm
              user = {this.state.user}
              onSubmitUpdateHandler = {this.onSubmitUpdateHandler}
              handleCancel={this.handleCancel}
              onSubmitDeleteHandler = {this.onSubmitDeleteHandler}
            />
          </Card>
        </Content>
        <FooterNav></FooterNav>
      </Layout>
        
      
    )
  }
     
}

const mapStateToProps = state =>{
  return{
    user:state.user,
    materials:state.materials
  }
}

export default connect(mapStateToProps)(Dashboard);
import React,{Component} from 'react'
import {Button} from 'antd'

export default class MaterialsForm extends Component{
  constructor(props){
    super(props)
  }

  componentDidMount(){
    console.log("props",this.props)
  }

  style ={
    border: "1px solid #dddddd",
    textAlign: "left",
    padding: "8px"
  }
  

  render(){
    return(
      <div>
       
        <table style = {{fontFamily:'arial', borderCollapse:'collapse',width:'100%'}}>
          <tbody>
            <tr>
              <th style = {this.style}>File Description</th>
              <th style = {this.style}>Download</th>
              <th style = {this.style}>Remove</th>
            </tr>
            
            {this.props.matData.map(item=>(
              <tr key={item.Company}>
                <td style = {this.style}>Filename: {item.Name} and company is {item.Company}.</td> 
                <td style = {this.style}>
                  <a href = {item.DataURL} download ={item.Company}>
                    <Button type="primary" >
                      Download QR
                    </Button>
                  </a>
                </td>
                <td style = {this.style}>
                <Button onClick = {()=> this.props.DeleteFile(item)}>
                    Remove
                </Button>
                </td>
              </tr>
              ))}
          </tbody>
        </table>

        <Button type="primary" onClick = {() => this.props.DownloadAll()} >
          Download All
        </Button>
      </div>
    )
  }


}
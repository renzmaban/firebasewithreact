import React,{ Component } from 'react';
import {Input,Form,Button} from 'antd'

export default class userModal extends Component {
  constructor(props){
    super(props)
    this.state = {
     userInput:{
      name:'',
      email:'',
      id:''
     }
    }
  }

  onChangeHandler = (e) =>{
    let {userInput} = this.state;
    
    switch(e.target.name){
      case 'uName':
      this.setState({userInput:{...userInput,name:e.target.value,id:this.props.id}})
      console.log("name",this.state.userInput.name);
      break;
      case 'uEmail':
      this.setState({userInput:{...userInput,email:e.target.value,id:this.props.id}})
      console.log("email",this.state.userInput.email);
      break;
    }

  }
  
  render(){
    return(
      <div>
        <Form className = 'example-input' onSubmit={(e)=>this.props.onSubmitUpdateHandler(e, this.state.userInput,this.props)}>
          <Input style={{"marginTop":"10px"}} addonBefore = "Name:" onChange={this.onChangeHandler} size="large" name='uName' placeholder="Input Name" />
          <Input style={{"marginTop":"10px"}} addonBefore = "Email:" onChange={this.onChangeHandler} size="large" name='uEmail' placeholder="Input Email Address" />
          <Button style={{"marginTop":"30px"}} type="primary" htmlType='submit' block>Submit</Button>
          <Button key="cancel" onClick={this.props.handleCancel}>Cancel</Button>
        </Form>
      </div>
    )
  }

}
import React, {Component} from 'react';
import RegistrationForm from './Form/registrationForm';
import { Layout,Card} from 'antd';
import 'antd/dist/antd.css';
import HeaderNav from './Layout/Header';
import FooterNav from './Layout/Footer';
import {dbFirebase} from '../config';
import {openNotif} from '../utils'

const { Content } = Layout;



export default class Registration extends Component{
  constructor(props){
    super(props);
  }
  
  onSubmitHandler =(e,userEntries) =>{
    e.preventDefault();
    let random = Math.floor((Math.random() * 100) + 1);
    console.log("test data",userEntries);
      dbFirebase.ref('user/' + random).set({
        name: userEntries.name,
        email: userEntries.email,
        id:random
      });
      openNotif('success',"User Registration Successful!","New user has been added!")
    }

  

  render(){
    return(
      <Layout>
        <HeaderNav/>  
        <Content style={{ padding: '0 10%', marginTop: '150px' ,marginLeft: "auto",  marginRight: "auto"}}>
          <Card
            title="Register"
            style={{ width: '100%' ,  marginLeft: "auto",  marginRight: "auto"}}
          >
            <RegistrationForm
              onSubmitHandler={this.onSubmitHandler}
            />
          </Card>
         
        </Content>
        <FooterNav/>
      </Layout>
    )
  }

}
import React,{Component} from 'react'
import LoginForm from './Form/loginForm'
import { Layout,Card} from 'antd';
import {authFirebase} from '../config'
// import axios from 'axios'
import 'antd/dist/antd.css';
import HeaderNav from './Layout/Header';
import FooterNav from './Layout/Footer';
import {Redirect} from 'react-router-dom'
import {openNotif,createTokens} from '../utils';
import { auth } from 'firebase';
import {connect} from 'react-redux'
import {updateUser} from '../js/Actions/userActions'
import decode from 'jwt-decode'

const { Content } = Layout;

class Login extends Component{
  constructor(props){
    super(props)
    this.state ={
      isValid:''
    }
  }
  

  onSubmitHandler = (event,userEntries,formProps) => {
    event.preventDefault();
    console.log("user Entries",userEntries);


    formProps.form.validateFields((err, values) => {
      let loggedInObj = {
        email:null,
        name:null,
        uid:null
      }
      if (!err) {
        console.log('Received values of form: ', values);
        authFirebase.signInWithEmailAndPassword(userEntries.username, userEntries.password)
        .then(authUser =>{
          sessionStorage.setItem("logged-in", true);
          console.log("auth user",authUser);
 
          loggedInObj=[
            authUser.user.email,
            authUser.user.displayName,
            authUser.user.uid
          ]

          this.props.onUpdateUser(loggedInObj);
          openNotif('success',"LogIn Successful!","Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum ")
          
          let sessiontoken = createTokens(loggedInObj)
          sessionStorage.setItem("user-session", sessiontoken);
          
          this.setState({isValid:"Success"});

        }).catch(function(err) {  
          
          // Handle Errors here.
          var errorCode = err.code;
          var errorMessage = err.message;
          openNotif('error',"LogIn Failed",err.message);
          console.log("error",err)
          // ...
        });
      }
    });


    
  };



  render(){
    {
      if(this.state.isValid =="Success"){
		  	return (<Redirect to ="/"/>)
      }
      console.log("test redux", this.props.user)
    }
    return(
      <Layout>
        <HeaderNav></HeaderNav>
       
        <Content style={{ padding: '0 10%', marginTop: '150px' ,marginLeft: "auto",  marginRight: "auto", width:"100%"}}>
        
          <Card
            title="Login"
            style={{ width: '100%' ,  marginLeft: "auto",  marginRight: "auto" , maxWidth:"500px"}}
          >
            <LoginForm
              onSubmitHandler ={this.onSubmitHandler}
            />
            
          </Card>
        </Content>
        <FooterNav></FooterNav>
      </Layout>
    )
  }


} 

const mapStateToProps = state =>{
  return{
    user:state.user,
    materials:state.materials
  }
}

const mapActionsToProps = {
  onUpdateUser:updateUser
};

export default connect(mapStateToProps,
  mapActionsToProps)(Login);

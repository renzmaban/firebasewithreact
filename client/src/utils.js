import {notification} from 'antd'
import jwt from 'jsonwebtoken'

const openNotif = (type,msg,desc) => {
  notification[type]({
    message: msg,
    description: desc,
  });
};

let createTokens = function(user){
  const sessiontoken = jwt.sign(
    {
      email:user[0],
      name:user[1],
      uid:user[2]
    },
    "secretTest",
    {
      expiresIn:'20m'
    }
  );
  return sessiontoken;
}


const serverhost = 'http://localhost:3000';

export{serverhost,openNotif,createTokens};
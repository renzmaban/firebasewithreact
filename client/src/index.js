import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {createStore,combineReducers} from 'redux'
import userReducers from './js/Reducers/userReducers'
import materialsReducer from './js/Reducers/materialsReducer'
import { Provider } from 'react-redux';


const allReducers = combineReducers({
  materials: materialsReducer,
  user: userReducers
});

const store = createStore(
  allReducers,
  {
      materials: { },
      user: { }
  }
);

ReactDOM.render(<Provider store = {store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();

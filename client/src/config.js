import * as firebase from 'firebase';

//testProject config

 // Initialize Firebase
 var Config = {
    apiKey: "AIzaSyCw11nKFIagwb9FBZWoN-FI8nFES1TOxhM",
    authDomain: "testproject-e6819.firebaseapp.com",
    databaseURL: "https://testproject-e6819.firebaseio.com",
    projectId: "testproject-e6819",
    storageBucket: "testproject-e6819.appspot.com",
    messagingSenderId: "427673106170"
  };

//GANAPP config

// var Config = {
//   apiKey: "AIzaSyAJuLLcSZoJzpXdkUlmgfNNCrtx-pTk98g",
//   authDomain: "eesy-15d1a.firebaseapp.com",
//   databaseURL: "https://eesy-15d1a.firebaseio.com",
//   projectId: "eesy-15d1a",
//   storageBucket: "eesy-15d1a.appspot.com",
//   messagingSenderId: "866486502831"
// };

  firebase.initializeApp(Config);

  const dbFirebase = firebase.database();
  const authFirebase = firebase.auth();
  const storageFirebase = firebase.storage();

export {dbFirebase,authFirebase,storageFirebase}